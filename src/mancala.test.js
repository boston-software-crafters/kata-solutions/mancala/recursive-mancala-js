import { mancala } from './mancala';

describe('mancala', () => {
  it('distribute a single stone', () => {
    let start_state = {
      player: 0,
      board: [1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    };
    let end_state = {
      player: 1,
      board: [0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    };
    expect(mancala(0, start_state)).toEqual(end_state);
  });
  it('distributes multiple stones', () => {
    let start_state = {
      player: 0,
      board: [2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    };
    let end_state = {
      player: 1,
      board: [0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    };
    expect(mancala(0, start_state)).toEqual(end_state);
  });
  it('Opponent can distribute stones', () => {
    expect(
      mancala(7, {
        player: 1,
        board: [4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 0],
      })
    ).toEqual({
      player: 0,
      board: [4, 4, 4, 4, 4, 4, 0, 0, 5, 5, 5, 5, 4, 0],
    });
  });
  it('wraps around', () => {
    expect(
      mancala(12, {
        player: 1,
        board: [4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 0],
      })
    ).toEqual({
      player: 0,
      board: [5, 5, 5, 4, 4, 4, 0, 4, 4, 4, 4, 4, 0, 1],
    });
  });
  it('skips the opponents store', () => {
    expect(
      mancala(5, {
        player: 0,
        board: [4, 4, 4, 4, 4, 9, 0, 4, 4, 4, 4, 4, 4, 0],
      })
    ).toEqual({
      player: 1,
      board: [5, 5, 4, 4, 4, 0, 1, 5, 5, 5, 5, 5, 5, 0],
    });
    expect(
      mancala(12, {
        player: 1,
        board: [4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 9, 0],
      })
    ).toEqual({
      player: 0,
      board: [5, 5, 5, 5, 5, 5, 0, 5, 5, 4, 4, 4, 0, 1],
    });
  });

  it('takes an extra turn if your last piece lands in your own mancala', () => {
    expect(
      mancala(2, {
        player: 0,
        board: [4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 0],
      })
    ).toEqual({
      player: 0,
      board: [4, 4, 0, 5, 5, 5, 1, 4, 4, 4, 4, 4, 4, 0],
    });
  });

  it('If the last piece you drop is in an empty pocket on your side, you capture that piece and any pieces in the pocket directly opposite', () => {
    expect(
      mancala(1, {
        player: 0,
        board: [4, 4, 4, 4, 4, 0, 0, 4, 4, 4, 4, 4, 4, 0],
      })
    ).toEqual({
      player: 1,
      board: [4, 0, 5, 5, 5, 0, 5, 0, 4, 4, 4, 4, 4, 0],
    });
  });
  it('The game ends when all six pockets on one side of the Mancala board are empty. The player who still has pieces on his/her side of the board when the game ends captures all of those pieces.', () => {
    expect(
      mancala(5, {
        player: 0,
        board: [0, 0, 0, 0, 0, 5, 3, 4, 4, 4, 4, 4, 4, 9],
      })
    ).toEqual({
      player: 1,
      board: [0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 37],
    });
  });
});
